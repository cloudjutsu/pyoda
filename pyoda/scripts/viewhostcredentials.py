
import pyoda.scripts.script as script

def run():
  s = script.Script()
  s.addOption( "-i", "--id", help="ID of host" )
  s.parse()
  org = s.getOrganization()
  host = org.getHost(s.getOptions()["id"])
  import pprint
  pprint.pprint( host.getSecrets() )


