
import pyoda.scripts.script as script

def run():
  s = script.Script()
  s.addOption( "-i", "--unique_id", help="Host unique name (serial number, vmid...)" )
  s.parse()
  org = s.getOrganization()
  host = org.newHost()
  host.autoFill()
  host.fillFromDict( s.getOptions() )
  if not host.data[ "unique_id" ]:
    raise pyoda.err.Error( "Missing host unique id" )
  host.saveAsManualHost()

