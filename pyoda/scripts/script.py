
import yaml
import types
import optparse
import pyoda.auth
import pyoda.organization
import pyoda.err

class Script:

  def __init__( self ):
    self.__parser = optparse.OptionParser()
    self.__data = { "url" : "https://oda.cloudjutsu.com"}
    self.__args = []
    self.__parser.add_option( "-e", "--email", action="store", help="Email of the user", dest="email" )
    self.__parser.add_option( "-t", "--api_token", action="store", help="API token to connect to oda", dest="api_token" )
    self.__parser.add_option( "-u", "--url", action = "store", help="Location of the oda instance to connect to", dest="url" )
    self.__parser.add_option( "-o", "--organizaion", action="store", help="Organizatio name", dest="organization" )
    self.__parser.add_option( "-P", "--password", action="store", help="Password required to access ciphered credentials" )
    self.__parser.add_option( "-c", "--conf_file", action="store", help="YAML configuration file to load", dest="conf_file" )

  def addOption( self, *args, **kwargs ):
    self.__parser.add_option( *args, **kwargs )

  def __loadConf( self, fname ):
    with open( fname ) as fd:
      self.__data.update( yaml.load( fd.read() ) )

  def parse( self ):
    opts, args = self.__parser.parse_args()
    if opts.conf_file:
      self.__loadConf( opts.conf_file)
    for k in [k for k in dir( opts ) if k[0] != "_" ]:
      v = getattr(opts,k)
      if isinstance(v,types.StringTypes):
        self.__data[k] = v
    self.__args = args

  def getOrganization(self):
    try:
      a = pyoda.auth.Auth( self.__data[ "email" ], self.__data[ "api_token" ] )
      return pyoda.organization.Organization( self.__data[ "organization" ], a, self.__data[ "url" ], self.__data[ "password" ] )
    except KeyError, e:
      raise pyoda.err.Error( "Missing configutaion {0}".format( e ) )

  def getOptions(self):
    return self.__data

  def getArgs(self):
    return self.__args

