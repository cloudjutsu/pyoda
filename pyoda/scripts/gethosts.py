
import pyoda.scripts.script as script

def run():
  s = script.Script()
  s.addOption( "-i", "--unique_id", help="Host unique name (serial number, vmid...)" )
  s.parse()
  org = s.getOrganization()
  hosts = org.getHosts()
  for h in hosts:
    print h
